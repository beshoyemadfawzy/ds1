![](Joker.jpg)

Name    : **Youssef Ahmed ELashry**

Mobile  : **01212497961** 

Email   : **youssefelashry66@gmail.com**

Address : **Mit Mahmoud, Mansoura, Dakahlia, Egypt .**


**Interests**
- Programming.
- Learning online.
- Whatching English movies. 


**Work Experience**
- Designing aluminum kitchens, doors and windows .


**Education**
- Biomedical Engineering Student Cairo University 2015-2020 .
- Tanah Secondary School Student 2012-2015 .


**Courses**
- Embedded Systems.
- Arduino.
- C language.
- C++ language.
- Electronics.
- Soft Skills.


**Activities**
- BEAT,Cairo University,Public Relation Member 2018.
- Energia powered,Cairo University,Marketing Member, 2018.


**Computer Skills**
- Microsoft Office.
- Circuits Design using Proteus,Circuit wizard and Multisim.


**Interpersonal Skills**
- Leadership.
- Presentation Skills.
- Teamwork.
- Problem Solving.
- Scientific Thinking.
