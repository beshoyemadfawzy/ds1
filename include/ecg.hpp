#ifndef ECG_HPP
#define ECG_HPP

#include "arrays.hpp"

namespace ecg
{
using ECGArray = arrays::DoubleArray;
struct Statistics
{

    double mean;
    double variance;
    double min;
    double max;
};
void analyzeECG(double *base, int arraySize, double &mean, double &variance, double &max, double &min)
{
    mean = arrays::meanArray(&base[0], arraySize);
    variance = arrays::varianceArray(&base[0], arraySize);
    max = arrays::maxArray(&base[0], arraySize);
    min = arrays::minArray(&base[0], arraySize);
}
Statistics analyzeECG(ECGArray ecg)
{
    Statistics stats = {0, 0, 0, 0};
    stats.mean = arrays::meanArray(&ecg.base[0], ecg.arraySize);
    stats.variance = arrays::varianceArray(&ecg.base[0], ecg.arraySize);
    stats.max = arrays::maxArray(&ecg.base[0], ecg.arraySize);
    stats.min = arrays::minArray(&ecg.base[0], ecg.arraySize);
    return stats;
}
}

#endif // ECG_HPP
