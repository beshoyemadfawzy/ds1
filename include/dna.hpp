#ifndef DNA_HPP
#define DNA_HPP

#include "arrays.hpp"

namespace dna 
{char complementaryBase( char base )
{
	switch ( base )
{
    	case 'A' :
    		{ return 'T'; } break ;
    
	case 'T' :
    		{ return 'A'; } break ;
    
	case 'C' :
    		{ return 'G'; } break ;
    	
	default  :
    		{ return 'C'; } break ;
						}}
 
   char * complementarySequence( char *base, int size )
{
	char *com = new char [size] ;
	
		
	for ( int i=0 ; size > 0 ; ++i )
		{ com[i] = complementaryBase(base[size-1]) ;
		  size -= 1  ; }

	return com ;

}

char *analyzeDNA( char *base, int size, int &countA, int &countC, int &countG, int &countT )
{
	countA = arrays::countCharacter( base , size , 'A' ) ;
	countC = arrays::countCharacter( base , size , 'C' ) ;
	countG = arrays::countCharacter( base , size , 'G' ) ;
	countT = arrays::countCharacter( base , size , 'T' ) ;
	
	return complementarySequence( base, size ) ;
}
}

#endif // DNA_HPP
