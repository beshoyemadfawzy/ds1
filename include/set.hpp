//
// Created by asem on 01/04/18.
//

#ifndef SBE201_WORDCOUNT_MAPS_SET_HPP
#define SBE201_WORDCOUNT_MAPS_SET_HPP

#include "bst.hpp"

namespace set
{

using WordSet = bst::BSTNode*;


WordSet create()
{
    return nullptr;
}

bool isEmpty( WordSet &wset )
{

    return wset == nullptr;

}

int size( WordSet &wset )
{

 if ( !set::isEmpty( wset))
        return 1 + set::size(wset->left ) + set::size( wset->right );
    else 
        return 0;

  
}
   

bool contains( WordSet &wset , std::string item )
{

  if ( set::isEmpty( wset ))
        return false;

  if ( set::isEmpty( wset ))
   {
     std::cout <<"item isnot found" <<std::endl;
   }

    else
    {
         return bst::find(wset,item);
    }

}

void remove( WordSet &wset , std::string to_remove )
{
   if ( set::isEmpty( wset  )) return;

    if (  to_remove == wset -> item )
    {
        if ( !set::isEmpty( wset ->left ) && !set::isEmpty( wset ->right ))
        {
            WordSet minRight = minNode(wset ->right );
            wset -> item= minRight-> item;
            set::remove( wset ->right, minRight-> item );
        }
         else
        {
            WordSet discard = wset ;

            if ( isLeaf(wset  ))
               wset  = nullptr;
            else if ( !set::isEmpty( wset ->left ))
               wset  = wset ->left;
            else
               wset  =wset ->right;

            delete discard;
        }

    } else if (  to_remove < wset -> item )
        set::remove( wset ->left,  to_remove );
    else set::remove( wset ->right,  to_remove );
}

void insert( WordSet &wset , std::string new_item )
{
    if( ! bst::find( wset , new_item))
        bst::insert( wset , new_item );
}

void printAll( WordSet &wset )
{
    if( wset )
    {
        printAll( wset->left );
        std::cout << wset->item << std::endl;
        printAll( wset->right );
    }
}

 void populate (WordSet &set1,WordSet &set2)
{
 populate(set1->left , set2);
 populate (set1->right ,set2);
 set::insert (set2 ,set1 ->item );
}
WordSet union_( WordSet &set1 , WordSet &set2 )
{
  WordSet set3 = create();
    set::populate(set1,set3);
    set::populate(set2,set3);
    return set3;

}
void intersection (WordSet &set1,WordSet &set2,WordSet &set4)
 {
     intersection(set1->left, set2, set4);
     intersection(set1->right, set2, set4);
     if (contains(set2,set1->item))
     {
         set::insert (set4, set1->item);
     }
 }


WordSet intersection( WordSet &set1 , WordSet &set2 )
{
    WordSet set4 =create();
    set::intersection(set1, set2, set4);
    return set4;

}


bool equals( WordSet &set1 , WordSet &set2 )
{
  return set1->item == set2->item;
}
}
#endif //SBE201_WORDCOUNT_MAPS_SET_HPP
